/**
 * Created by danielxiao on 15/9/19.
 */

angular.module('pocket-preferential-shopping-app.services')

  .factory('startService', ['$rootScope', '$q', 'localStoreService', 'appConstants', 'appGlobalVal', 'daoService',

    function($rootScope, $q, localStoreService, appConstants, appGlobalVal, daoService) {

      return {
        appStart: function() {
          _init();
        }
      };

      function _init() {

        daoService.initData();

        appGlobalVal.settings = localStoreService.get(appConstants.localStorage.settings.key);

        if (!appGlobalVal.settings) {

          var language = appConstants.languages[0]; // TODO Daniel: 浏览器调度用，正式注释换成上面的

          $rootScope.i18n = window[language.key];

          appGlobalVal.settings = { // 默认设置
            language  : language
          };

          localStoreService.set(appConstants.localStorage.settings.key, appGlobalVal.settings);

        } else {

          $rootScope.i18n = window[appGlobalVal.settings.language.key];

        }

        // 当APP启动的时候才上传错误日志
        uploadErrorMsgs();
      }

      function uploadErrorMsgs() {

        var sendToRemote = function(data) {

          var deferred = $q.defer();

          var xmlhttp = new XMLHttpRequest();

          xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
              if (xmlhttp.status === 200) {
                var ids = _.map(data, function(item) { return item.body.id; });
                daoService.updateErrorMsgsSyncStatus(ids);
              }
              deferred.resolve();
            }
          };
          xmlhttp.open("POST", 'https://api.bmob.cn/1/batch', true);
          xmlhttp.setRequestHeader("X-Bmob-Application-Id", "a9f74f8b92f214fa5e4c596773df8610");
          xmlhttp.setRequestHeader("X-Bmob-REST-API-Key", "aecdf73ce714a68ac431821170fb3086");
          xmlhttp.setRequestHeader("Content-Type", "application/json");
          xmlhttp.send(JSON.stringify({
            "requests": data
          }));

          return deferred.promise;
        };

        daoService.getUnSyncErrorMsgs().then(function(data) {
          var dataList = _.chain(data).chunk(20).map(function(dataItem) {
            return _.map(dataItem, function(item) {
              return {
                "method": "POST",
                "path"  : "/1/classes/error_msg",
                "body"  : item
              }
            })
          }).value();

          dataList.reduce(function(chain, errorData) {
            return chain.then(function() {
              return sendToRemote(errorData);
            })
          }, $q.resolve());
        });
      }

    }]);
