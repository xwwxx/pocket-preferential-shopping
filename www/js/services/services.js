angular.module('pocket-preferential-shopping-app.services', [])

  .factory('daoService', ['$rootScope', '$cordovaSQLite', '$cordovaLocalNotification', '$q',

    function($rootScope, $cordovaSQLite, $cordovaLocalNotification, $q) {

      var db;

      var getDb = function() {

        if (db) return db;

        if (window.cordova) {
          db = $cordovaSQLite.openDB({name: "preferential-shopping.db", bgType: 1}); //device
        } else {
          db = window.openDatabase("preferential-shopping.db", '1', 'preferential-shopping', 1024 * 1024 * 100); // browser
        }
        return db;
      };

      return {

        initData: function() {
          var sqls = [
            "CREATE TABLE IF NOT EXISTS error_msg (id INTEGER PRIMARY KEY AUTOINCREMENT, errorUrl TEXT, errorMessage TEXT, stackTrace TEXT, hasSync INTEGER)"
          ];

          var promises = _.map(sqls, function(sql) {
            return $cordovaSQLite.execute(getDb(), sql);
          });

          return $q.all(promises);
        },

        addErrorMsg: function(errorMsgItem) {
          var id = genId();
          var sql = 'INSERT INTO error_msg VALUES (?, ?, ?, ?, ?)';
          return $cordovaSQLite.execute(getDb(), sql, [id, errorMsgItem.errorUrl || '', errorMsgItem.errorMessage || '', errorMsgItem.stackTrace || '', 0]).then(function() {
            return id;
          });
        },

        isErrorMsgExist: function(errorUrl, errorMessage) {
          var sql = 'SELECT COUNT(id) total FROM error_msg WHERE errorUrl=? AND errorMessage=?';
          return $cordovaSQLite.execute(getDb(), sql, [errorUrl, errorMessage]).then(function(data) {
            return data.rows.item(0).total > 0;
          });
        },

        getUnSyncErrorMsgs: function() {
          var sql = 'SELECT id, errorUrl, errorMessage, stackTrace FROM error_msg WHERE hasSync=0';
          return $cordovaSQLite.execute(getDb(), sql).then(function(data) {
            var items = [];
            for (var i = 0, leng = data.rows.length; i < leng; i++) {
              var item = data.rows.item(i);
              items.push(item);
            }
            return items;
          });
        },

        updateErrorMsgsSyncStatus: function(ids) {
          var sql = 'UPDATE error_msg SET hasSync=1 WHERE id in (' + ids.join(',') + ')';
          return $cordovaSQLite.execute(getDb(), sql);
        }
      };

      function genId() {
        return new Date().getTime();
      }

    }]);
