/**
 * Created by danielxiao on 15/9/28.
 */

angular.module('pocket-preferential-shopping-app.rest-services', [])

  .factory('restService', ['$q', 'localStoreService', 'daoService', 'appConstants', 'appGlobalVal', 'utilService',

    function($q, localStoreService, daoService, appConstants, appGlobalVal, utilService) {

      var serviceInstance = {

        /**
         *
         * @param keyword
         * @param pt
         * @param pid 追踪ID
         * @param ept
         * @param cid 分类ID
         * @param vcid 子分类ID
         * @param pageNum
         * @returns {*}
         */
        getProductList: function(keyword, pt, pid, ept, cid, vcid, pageNum) {
          return utilService.httpGet('http://temai.m.taobao.com/cheap/items.json', _.omit({
            page: pageNum,
            keyword: keyword,
            pt: pt,
            pid: pid,
            ept: ept,
            cid: cid,
            vcid: vcid
          }, function(item) { return item === undefined; }), {timeout: 10000});
        }

      };

      return serviceInstance;

    }]);
