/**
 * Created by danielxiao on 15/9/5.
 */

angular.module('pocket-preferential-shopping-app.controllers')

  .controller('ProductCategoriesCtrl', ['$scope', '$ionicPlatform',

    function($scope, $ionicPlatform) {

      var deregisterBackBtnAction;

      var selectedCateItem;

      /*========== Scope Models ==================================================*/

      $scope.categories = [];

      $scope.subCategories = [];

      /*========== Scope Functions ==================================================*/

      $scope.selectCategory = function(item) {
        _.each($scope.categories, function(cateItem) { cateItem.selected = false; });
        item.selected = true;

        selectedCateItem = item;
        $scope.subCategories = item.subCates;
      };

      $scope.selectSubCategory = function(item) {
        _.each($scope.subCategories, function(cateItem) { cateItem.selected = false; });

        item.selected = true;

        $scope.$close({
          cid: item.cid,
          vcid: item.vcid,
          keyword: item.keyword,
          cateName: selectedCateItem.cName + (item.vcid === 0 ? '' : '-' + item.vcName)
        });

      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('modal.shown', function() {
        // 注册这个事件是为了防止第一次导入成功，再打开这个页面，然后按android的回退键会再次触发导入动作
        deregisterBackBtnAction = $ionicPlatform.registerBackButtonAction(function() {
          $scope.$close();
        }, 1000);

        _init();
      });

      $scope.$on('$destroy', function() {
        deregisterBackBtnAction && deregisterBackBtnAction();
      });

      /*========== Private Functions ==================================================*/

      function _init() {
        var selectedCateInfo = $scope.param_selected_cateInfo;
        $scope.categories = $scope.param_categories;
        _.some($scope.categories, function(item) {
          if (item.cid === selectedCateInfo.cid) {
            $scope.selectCategory(item);
            _.some(item.subCates, function(subItem) {
              if (subItem.vcid === selectedCateInfo.vcid) {
                subItem.selected = true;
                return true;
              }
            });
            item.selected = true;
            return true;
          }
        })
      };

    }]);
