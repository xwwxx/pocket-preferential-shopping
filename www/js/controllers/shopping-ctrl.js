angular.module('pocket-preferential-shopping-app.controllers')

  .controller('ShoppingCtrl', ['$scope', '$state', '$ionicScrollDelegate', 'restService', 'ionicModal',

    function($scope, $state, $ionicScrollDelegate, restService, ionicModal) {

      var searchCondition = {
        pageNum: 0 // 当前页码 0是因为一开始就会运行loadMore
      };

      var categories = window.productCategories;

      var pt, ept, pid;

      /*========== Scope Models ==================================================*/

      $scope.activeMenuTabIndex = 0;

      $scope.menuOpen = 'closed';

      $scope.noMoreData = false;

      $scope.isNoData = false;

      $scope.loadingData = false;

      $scope.dataList = [];

      $scope.cateInfo = {
        cid    : 0,
        vcid   : 0,
        keyword: window.productCategories[0].subCates[0].keyword
      };

      $scope.title = $rootScope.i18n.leisureMoment;

      /*========== Scope Functions ==================================================*/

      $scope.changeTab = function(idx, param_pt, param_ept) {
        $scope.activeMenuTabIndex = idx;
        pt = param_pt;
        ept = param_ept;
        $ionicScrollDelegate.scrollTop();
        $scope.doRefresh();
      };

      $scope.loadMore = function() {
        if (!$scope.loadingData) {
          searchCondition.pageNum++;
          _getDataList(true);
        }
      };

      $scope.doRefresh = function() {
        searchCondition.pageNum = 1;
        _getDataList(false);
      };

      $scope.showCategories = function() {
        ionicModal.productCategoriesModal.open($scope, {
          param_categories       : angular.copy(categories),
          param_selected_cateInfo: $scope.cateInfo
        }).then(function(data) {
          if (data && (_.get(data, 'cid') !== $scope.cateInfo.cid || _.get(data, 'vcid') !== $scope.cateInfo.vcid)) {
            $scope.cateInfo = data;
            $scope.title = $scope.cateInfo.cateName;
            $ionicScrollDelegate.scrollTop();
            $scope.doRefresh();
          }
        });
      };

      $scope.viewProductDetail = function(item) {
        ionicModal.productDetailModal.open($scope, {
          param_itemInfo: item
        });
      };

      $scope.goToFavorites = function() {
        $scope.menuOpen = 'closed';
        $state.go('my-favorites');
      };

      $scope.goToFootprints = function() {
        $state.go('my-footprints');
        $scope.menuOpen = 'closed';
      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('$ionicView.loaded', function() {
        _init();
      });

      /*========== Private Functions ==================================================*/

      function _getDataList(isAppend) {

        $scope.loadingData = true;
        $rootScope.loading(true);

        restService.getProductList($scope.cateInfo.keyword, pt, pid, ept, $scope.cateInfo.cid || undefined, $scope.cateInfo.vcid || undefined, searchCondition.pageNum).success(function(data) {
          $scope.loadingData = false;

          data.results = _.get(data, 'result.items', []);
          data.count = _.get(data, 'result.paged.items', 0);

          _.each(data.results, function(item) {
            item.pictUrl = item.pictUrl.replace(/^\/\//, 'http://') + '_310x310q90';
            item.tkUrl = item.tkUrl.replace(/^\/\//, 'http://');
            item.biz30day = item.biz30day >= 10000 ? (item.biz30day/10000).toFixed(1) + '万' : item.biz30day;
          });

          if (isAppend) {
            $scope.dataList = $scope.dataList.concat(data.results);
          } else {
            $scope.dataList = data.results;
          }

          // 设置是否load more可用
          if (data.results.length === 0 || data.count <= $scope.dataList.length) {
            $scope.noMoreData = true;
          } else {
            $scope.noMoreData = false;
          }
        }).catch(function(e) {
          //console.error(e);
        }).finally(function() {
          $rootScope.loading(false);
          $scope.isNoData = $scope.dataList.length === 0;
          $scope.$broadcast('scroll.refreshComplete');
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
      }

      function _init() {
        $scope.imageHeight = window.innerWidth / 2 - 15;
        $scope.itemHeight = $scope.imageHeight + 100;

        pt = $state.params.pt || 1;
        ept = $state.params.ept;

        var platform = ionic.Platform.isIOS() ? 'ios' : 'android';
        pid = platform == 'android' ? 'mm_112675308_11426331_40316680' : 'mm_112675308_11788217_41794679'; // 推广追踪ID

      };

    }]);
