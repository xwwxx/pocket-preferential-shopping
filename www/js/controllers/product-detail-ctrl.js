/**
 * Created by danielxiao on 15/9/5.
 */

angular.module('pocket-preferential-shopping-app.controllers')

  .controller('ProductDetailCtrl', ['$scope', '$rootScope', '$state', '$ionicSlideBoxDelegate', '$ionicPopup', 'appConstants', 'closePopupService', 'localStoreService',

    function($scope, $rootScope, $state, $ionicSlideBoxDelegate, $ionicPopup, appConstants, closePopupService, localStoreService) {

      var itemId;

      var urlMap = {
        taobao: 'http://hws.m.taobao.com/cache/wdetail/5.0/?id=',
        tmall : 'https://detail.m.tmall.com/item.htm?id='
      };

      /*========== Scope Models ==================================================*/

      $scope.productInfo = {};

      $scope.shoppingBtnWith = window.innerWidth - 160;

      /*========== Scope Functions ==================================================*/

      $scope.goToBuy = function() {
        $rootScope.openBrowser($scope.productInfo.tkUrl, {logTitle: 'shopping/goToBuy'});
      };

      $scope.addToMyFavorites = function() {
        $rootScope.remoteLog(appConstants.remoteLoggerInfo.action.title, 'product/addToMyFavorites');
        var favoritesList = localStoreService.get(appConstants.localStorage.favorites.key) || [];
        var foundOne = _.find(favoritesList, 'itemId', $scope.param_itemInfo.itemId);
        if (!foundOne) {
          if (favoritesList.length >= appConstants.localStorage.favorites.maxLength) { // 先进先出
            favoritesList = _.drop(favoritesList);
          }
          favoritesList.push({
            itemId: $scope.param_itemInfo.itemId,
            pictUrl: $scope.param_itemInfo.pictUrl,
            title: $scope.param_itemInfo.title,
            zkPrice: $scope.param_itemInfo.zkPrice,
            reservePrice: $scope.param_itemInfo.reservePrice,
            tkUrl: $scope.param_itemInfo.tkUrl,
            tmall: $scope.param_itemInfo.tmall,
            biz30day: $scope.param_itemInfo.biz30day,
            postfree: $scope.param_itemInfo.postfree
          });
          localStoreService.set(appConstants.localStorage.favorites.key, favoritesList);
        }

        $rootScope.alert($rootScope.i18n.favoriteSuccess);
      };

      $scope.popupSharePanel = function() {
        var popup = $ionicPopup.show({
          templateUrl: 'share-product-panel.html',
          title      : $rootScope.i18n.shareProduct,
          scope      : $scope
        });
        closePopupService.register(popup);
      };

      $scope.shareToWeChat = function() {
        WeChat
          .share({
            type       : WeChat.ShareType.webpage,
            title      : $scope.productInfo.productName,
            description: $rootScope.i18n.discountPrice + ':' + ($scope.productInfo.postfree ? $rootScope.i18n.postFree : '') + $scope.productInfo.zkPrice + '\n' + $rootScope.i18n.originPrice + ':' + $scope.productInfo.rePrice,
            url        : $scope.productInfo.tkUrl
          }, WeChat.Scene.session, function() {
            $rootScope.remoteLog(appConstants.remoteLoggerInfo.action.title, 'product/shareToWeChat');
          }, function() {
            $rootScope.remoteLog(appConstants.remoteLoggerInfo.action.title, 'product/shareToWeChatFailed');
          });
      };

      $scope.shareToQQ = function() {
        var args = {
          url        : $scope.productInfo.tkUrl,
          title      : $scope.productInfo.productName,
          description: $rootScope.i18n.discountPrice + ':' + ($scope.productInfo.postfree ? $rootScope.i18n.postFree : '') + $scope.productInfo.zkPrice + '\n' + $rootScope.i18n.originPrice + ':' + $scope.productInfo.rePrice,
          imageUrl   : $scope.productInfo.pictUrl,
          appName    : $rootScope.i18n.appName
        };
        YCQQ.shareToQQ(function() {
          $rootScope.remoteLog(appConstants.remoteLoggerInfo.action.title, 'product/shareToQQ');
        }, function() {
          $rootScope.remoteLog(appConstants.remoteLoggerInfo.action.title, 'product/shareToQQFailed');
        }, args);
      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('modal.shown', function() {
        _init();

        // 记录足迹
        var footprintsList = localStoreService.get(appConstants.localStorage.footprints.key) || [];
        if (_.get(_.last(footprintsList), 'itemId') !== $scope.param_itemInfo.itemId) { // 当最后一个与当前不一样时才记录
          if (footprintsList.length >= appConstants.localStorage.footprints.maxLength) { // 先进先出
            footprintsList = _.drop(footprintsList);
          }
          footprintsList.push({
            itemId: $scope.param_itemInfo.itemId,
            pictUrl: $scope.param_itemInfo.pictUrl,
            title: $scope.param_itemInfo.title,
            zkPrice: $scope.param_itemInfo.zkPrice,
            reservePrice: $scope.param_itemInfo.reservePrice,
            tkUrl: $scope.param_itemInfo.tkUrl,
            tmall: $scope.param_itemInfo.tmall,
            biz30day: $scope.param_itemInfo.biz30day,
            postfree: $scope.param_itemInfo.postfree
          });
          localStoreService.set(appConstants.localStorage.footprints.key, footprintsList);
        }

      });

      /*========== Private Functions ==================================================*/


      function _getTmallDetail(htmlContent) {

        var $content = $(htmlContent);

        // 取是图片信息
        var imgs = $content.find('.itbox img');
        var imgUrls = _.map(imgs, function(item) {
          return ($(item).attr('src') || $(item).data('src')).replace(/^\/\//, 'http://');
        });
        $scope.productInfo.imgUrls = imgUrls;

        // 取得详情
        var matchData = $content.text().match(/httpsDescUrl":"(.*?)"/);
        var descUrl = _.get(matchData, '[1]').replace(/^\/\//, 'http://');

        $.ajax({
          url: descUrl
        }).done(function(content) {
          // 返回来是个JS的变量值（var desc='<p>...</p>'），所以把JS部分替换掉
          content = content.replace(/.*?'/, '').replace(/';/, '');
          $scope.productInfo.htmlContent = content;
        });
      }

      function _getTaoBaoDetail(data) {

        // 取是图片信息
        $scope.productInfo.imgUrls = data.data.itemInfoModel.picsPath;

        // 取得详情
        var descUrl = data.data.descInfo.briefDescUrl;
        $.ajax({
          url: descUrl
        }).done(function(content) {
          var imgList = _.map(content.data.images, function(imgUrl) {
            return '<img src="' + imgUrl + '">';
          });

          $scope.productInfo.htmlContent = '<div>' + imgList.join('') + '</div>';
        });

      }

      function _init() {

        $rootScope.loading(true);

        itemId = $scope.param_itemInfo.itemId;
        var isTmall = $scope.param_itemInfo.tmall;
        var productUrl = (isTmall ? urlMap.tmall : urlMap.taobao) + itemId;

        $scope.productInfo.productName = $scope.param_itemInfo.title; // 商品名称
        $scope.productInfo.rePrice = $scope.param_itemInfo.reservePrice; // 原来价格
        $scope.productInfo.zkPrice = $scope.param_itemInfo.zkPrice; // 折扣价格
        $scope.productInfo.biz30day = $scope.param_itemInfo.biz30day; // 月销量
        $scope.productInfo.postfree = $scope.param_itemInfo.postfree; // 是否包邮
        $scope.productInfo.tkUrl = $scope.param_itemInfo.tkUrl; // 商品在天猫/淘宝的URL

        $.ajax({
          url: productUrl,
          timeout: 10 * 1000
        }).done(function(htmlContent) {

          if (isTmall) {
            _getTmallDetail(htmlContent);
          } else {
            _getTaoBaoDetail(htmlContent);
          }

          setTimeout(function() {
            $ionicSlideBoxDelegate.update();
            $scope.$apply();
          }, 1);

        }).always(function() {
          $rootScope.loading(false);
        });
      }

    }]);
