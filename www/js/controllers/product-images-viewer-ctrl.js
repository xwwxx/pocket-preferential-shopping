/**
 * Created by danielxiao on 15/3/5.
 */

(function() {

  angular.module('pocket-preferential-shopping-app.controllers').controller('ProductImagesViewerCtrl', ['$scope',

    function($scope) {

      /*========== Scope Models ==================================================*/

      $scope.photos = [];

      $scope.currentShowItemIdx = $scope.param_itemIdx;

      /*========== Scope Functions ==================================================*/

      $scope.toggleEditMode = function() {
        $scope.editMode = !$scope.editMode;
      };

      $scope.cancel = function() {
        $scope.$close();
      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('modal.shown', function() {
        _init();
      });

      /*========== Private Functions ==================================================*/

      function _init() {
        $scope.photos = _.uniq($scope.param_photos);
      };

    }
  ]);

})();
