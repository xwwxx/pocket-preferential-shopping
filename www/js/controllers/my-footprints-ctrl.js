angular.module('pocket-preferential-shopping-app.controllers')

  .controller('MyFootprintsCtrl', ['$scope', '$rootScope', 'localStoreService', 'appConstants', 'ionicModal',

    function($scope, $rootScope, localStoreService, appConstants, ionicModal) {

      /*========== Scope Models ==================================================*/

      $scope.dataList = [];

      /*========== Scope Functions ==================================================*/

      $scope.viewProductDetail = function(item) {
        ionicModal.productDetailModal.open($scope, {
          param_itemInfo: item
        });
      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('$ionicView.enter', function() {
        _init();
      });

      /*========== Private Functions ==================================================*/

      function _getDataList() {
        $scope.dataList = localStoreService.get(appConstants.localStorage.footprints.key) || [];
        $scope.dataList.reverse(); // 倒序
      }

      function _init() {
        _getDataList();
      };

    }]);
