angular.module('pocket-preferential-shopping-app.controllers')

  .controller('MyFavoritesCtrl', ['$scope', '$rootScope', 'localStoreService', 'appConstants', 'ionicModal',

    function($scope, $rootScope, localStoreService, appConstants, ionicModal) {

      /*========== Scope Models ==================================================*/

      $scope.dataList = [];

      /*========== Scope Functions ==================================================*/

      $scope.rmProduct = function(idx, event) {
        event.stopPropagation();
        $rootScope.confirm($rootScope.i18n.rmFavoriteConfirm).then(function(isConfirm) {
          if (isConfirm) {
            $scope.dataList.splice(idx, 1);
            var favorites = angular.copy($scope.dataList);
            favorites.reverse();
            localStoreService.set(appConstants.localStorage.favorites.key, favorites);
          }
        });
      };

      $scope.viewProductDetail = function(item) {
        ionicModal.productDetailModal.open($scope, {
          param_itemInfo: item
        });
      };

      /*========== Listeners ==================================================*/

      /*========== Watches ==================================================*/

      $scope.$on('$ionicView.enter', function() {
        _init();
      });

      /*========== Private Functions ==================================================*/

      function _getDataList() {
        $scope.dataList = localStoreService.get(appConstants.localStorage.favorites.key) || [];
        $scope.dataList.reverse();
      }

      function _init() {
        _getDataList();
      };

    }]);
