// Ionic Starter App

/**
 * 这样可以确保设备ready，才启用device相关功能
 */
ionic.Platform.ready(function() {

  angular.bootstrap(document, ['pocket-preferential-shopping-app']);

});

/**
 *  This patch works around iOS9 UIWebView regression that causes infinite digest
 *  errors in Angular.
 */
angular.module('ngIOS9UIWebViewPatch', ['ng']).config(['$provide', function($provide) {
  'use strict';

  $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {

    if (isIOS9UIWebView($window.navigator.userAgent)) {
      return applyIOS9Shim($delegate);
    }

    return $delegate;

    function isIOS9UIWebView(userAgent) {
      return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
    }

    function applyIOS9Shim(browser) {
      var pendingLocationUrl = null;
      var originalUrlFn = browser.url;

      browser.url = function() {
        if (arguments.length) {
          pendingLocationUrl = arguments[0];
          return originalUrlFn.apply(browser, arguments);
        }

        return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
      };

      window.addEventListener('popstate', clearPendingLocationUrl, false);
      window.addEventListener('hashchange', clearPendingLocationUrl, false);

      function clearPendingLocationUrl() {
        pendingLocationUrl = null;
      }

      return browser;
    }
  }]);
}]);

angular.module('pocket-preferential-shopping-app', ['ionic', 'ngCordova', 'pasvaz.bindonce', 'LocalStorageModule', 'ngIOS9UIWebViewPatch', 'jett.ionic.scroll.sista', 'ng-mfb', 'ionicLazyLoad', 'common.service', 'modal.service', 'pocket-preferential-shopping-app.directive', 'pocket-preferential-shopping-app.filter', 'pocket-preferential-shopping-app.controllers', 'pocket-preferential-shopping-app.services', 'pocket-preferential-shopping-app.rest-services', 'pocket-preferential-shopping-app.exception-service'])

  .constant('appConstants', {
    appVersion      : '1.1.2',
    appDownloadUrl  : 'http://a.app.qq.com/o/simple.jsp?pkgname=top.danielxiao.pocket_fkc',
    languages       : [
      {
        id     : 1,
        display: '简体中文',
        key    : 'i18n_zh_cn'
      },
      {
        id     : 2,
        display: '繁體中文',
        key    : 'i18n_zh_hk'
      }
    ],
    // 本地存储键值及设置
    localStorage    : {
      settings  : {
        key: 'settings'
      },
      favorites : {
        key      : 'favorites',
        maxLength: 100
      },
      footprints: {
        key      : 'footprints',
        maxLength: 100
      }
    },
    // 远程日志
    remoteLoggerInfo: {
      pageView: { // 功能访问
        title: 'Page View'
      },
      action  : { // 动作
        title: 'Action'
      }
    }
  })

/**
 * 全局变量
 */
  .value('appGlobalVal', {
    settings: {}
  })

  .run(['$ionicPlatform', 'startService',

    function($ionicPlatform, startService) {

      startService.appStart();

      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true); // 注释掉避免IOS下拉框值没有DONE按钮
          cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleLightContent();
        }
      });
    }])

  .config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', '$compileProvider', 'localStorageServiceProvider', 'remoteLoggerProvider',

    function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider, localStorageServiceProvider, remoteLoggerProvider) {

      $ionicConfigProvider.backButton.text('');
      $ionicConfigProvider.backButton.previousTitleText('');
      $ionicConfigProvider.tabs.position('bottom');
      $ionicConfigProvider.navBar.alignTitle('center');

      remoteLoggerProvider.setTimeout(60 * 30);
      remoteLoggerProvider.setMaxLength(5);

      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|content):/);

      localStorageServiceProvider.setPrefix('pocket_preferential_shopping');

      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

        .state('shopping', {
          url        : '/shopping?pt&ept',
          templateUrl: 'templates/shopping-list.html',
          controller : 'ShoppingCtrl'
        })

        .state('my-favorites', {
          url        : '/my-favorites',
          templateUrl: 'templates/my-favorites-list.html',
          controller : 'MyFavoritesCtrl'
        })
        .state('my-footprints', {
          url        : '/my-footprints',
          templateUrl: 'templates/my-footprints-list.html',
          controller : 'MyFootprintsCtrl'
        })
      ;

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/shopping');

    }]);
